﻿using CitizensOf62Country.Enums;
using CitizensOf62Country.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CitizensOf62Country.Data
{
    /// <summary>
    /// A static class that store list of civilian data.
    /// </summary>
    public static class CivilianData
    {
        public static Dictionary<string, CivilianDataModel> Civilians { get; set; } = new Dictionary<string, CivilianDataModel>
        {
            // Add Jack data.
            {
                // This is his KTP.
                "JCK",
                new CivilianDataModel
                {
                    Name = "Jack",
                    Address = "Jakarta",
                    PhoneNumber = "+620000000",
                    Birthday = new DateTime(1990, 1, 1),
                    Gender = Gender.Male,
                    // UTC reference: https://en.wikipedia.org/wiki/Coordinated_Universal_Time.
                    CreatedAt = DateTimeOffset.UtcNow
                }
            },
            {
                "BRB",
                new CivilianDataModel
                {
                    Name = "Birb",
                    Address = "Bandung",
                    PhoneNumber = "+620000001",
                    Birthday = new DateTime(1995, 2, 12),
                    Gender = Gender.Male,
                    CreatedAt = DateTimeOffset.UtcNow
                }
            },
            {
                "PIP",
                new CivilianDataModel
                {
                    Name = "Pip",
                    Address = "Medan",
                    PhoneNumber = "+620000002",
                    Birthday = new DateTime(2000, 12, 5),
                    Gender = Gender.Female,
                    CreatedAt = DateTimeOffset.UtcNow
                }
            },
            {
                "NRT",
                new CivilianDataModel
                {
                    Name = "Naruto",
                    Address = "Jepara",
                    PhoneNumber = "+620005245",
                    Birthday = new DateTime(1989, 7, 2),
                    Gender = Gender.Male,
                    CreatedAt = DateTimeOffset.UtcNow
                }
            },
            {
                "SIT",
                new CivilianDataModel
                {
                    Name = "Siti",
                    Address = "Depok",
                    PhoneNumber = "+620000123",
                    Birthday = new DateTime(2000, 2, 29),
                    Gender = Gender.Female,
                    CreatedAt = DateTimeOffset.UtcNow
                }
            },
            {
                "JOCK",
                new CivilianDataModel
                {
                    Name = "Jock",
                    Address = "Papua",
                    PhoneNumber = "+620000010",
                    Birthday = new DateTime(2001, 5, 21),
                    Gender = Gender.Male,
                    CreatedAt = DateTimeOffset.UtcNow
                }
            }
        };

        /// <summary>
        /// Getter for obtaining the total data of civilian data.
        /// </summary>
        public static int TotalData
        {
            get
            {
                return Civilians.Count;
            }
        }
    }
}
