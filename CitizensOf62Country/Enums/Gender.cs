﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CitizensOf62Country.Enums
{
    /// <summary>
    /// Gender enum, since we only acknowledge 2 genders: male and female.
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// By default, Male value is 0.
        /// </summary>
        Male,
        /// <summary>
        /// Female value is automatically become 1.
        /// </summary>
        Female
    }
}
