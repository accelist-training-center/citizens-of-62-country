﻿using CitizensOf62Country.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CitizensOf62Country.Models
{
    /// <summary>
    /// Model class for storing civilian data.
    /// </summary>
    public class CivilianDataModel
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public Gender Gender { get; set; }

        public DateTime Birthday { get; set; }

        /// <summary>
        /// We will use <seealso cref="DateTimeOffset"/> instead of <seealso cref="DateTime"/>.
        /// Reference: https://docs.microsoft.com/en-us/dotnet/standard/datetime/choosing-between-datetime.
        /// </summary>
        public DateTimeOffset CreatedAt { get; set; }
    }
}
