﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CitizensOf62Country.Models
{
    /// <summary>
    /// Model class for displaying civilian data.
    /// </summary>
    public class CivilianModel
    {
        public string Ktp { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Gender { get; set; }

        public string Birthday { get; set; }

        public string CreatedAt { get; set; }
    }
}
