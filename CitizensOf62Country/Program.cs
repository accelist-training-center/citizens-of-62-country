using CitizensOf62Country.Data;
using CitizensOf62Country.Enums;
using CitizensOf62Country.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace CitizensOf62Country
{
    class Program
    {
        /// <summary>
        /// List of menu option that is available in this menu.
        /// </summary>
        public static List<string> MenuOptions { get; set; } = new List<string>
        {
            "1. Go to page",
            "2. Search",
            "3. Add new entry",
            "4. Update existing entry",
            "5. Delete existing entry",
            "6. Exit"
        };

        /// <summary>
        /// A constant value that store this application title.
        /// </summary>
        public const string ApplicationTitle = "Citizens of +62 Country";

        /// <summary>
        /// A constant value that store civilian data grid header string.
        /// </summary>
        private const string CivilianDataGridHeader = "| No. | KTP | Name | Address | Phone Number | Gender | Birthday | Created At |";

        /// <summary>
        /// A constant value that store Indonesia culture info ID.
        /// </summary>
        public const string IndonesiaCultureInfoId = "id-ID";

        /// <summary>
        /// Define the page index civilian data grid.
        /// </summary>
        public static int PageIndex { get; set; } = 1;

        /// <summary>
        /// Define the size of item per page.
        /// </summary>
        public const int ItemPerPage = 5;

        /// <summary>
        /// Define the size of data grid total data.
        /// </summary>
        public static int DataGridTotalData { get; set; } = 0;

        /// <summary>
        /// Store the search string value.
        /// </summary>
        public static string SearchString { get; set; }

        static void Main(string[] args)
        {
            DisplayMainMenu();
            AcceptInput();
        }

        /// <summary>
        /// Display the output of main menu contents.
        /// </summary>
        public static void DisplayMainMenu()
        {
            Console.WriteLine($"Welcome to {ApplicationTitle} system!");

            DisplayDataGrid();
            DisplayMenus();
        }

        /// <summary>
        /// Display the civilian data grid.
        /// </summary>
        public static void DisplayDataGrid()
        {
            Console.WriteLine(@$"
Civilian Data Grid (displaying {ItemPerPage} per page):");
            Console.WriteLine(CivilianDataGridHeader);

            var civilians = new List<CivilianModel>();

            // Check whether SearchString is null or empty (empty string = "").
            if (string.IsNullOrEmpty(SearchString) == true)
            {
                DataGridTotalData = CivilianData.TotalData;

                // Using LINQ's Select() method, create a list of CivilianModel from list of CivilianDataModel object.
                /*
                 * Skip() can be used to immediately skip to the supplied index parameter in the data list.
                 * Example: based on the formula (PageIndex - 1) * ItemPerPage, while PageIndex = 1 and ItemPerPage = 5,
                 * then it will not skip to any index because the result is 0.
                 * Another example: if PageIndex = 2 and ItemPerPage = 5, the result will be 5.
                 * So, it will skip to the 5th index of the data list.
                 */
                /*
                 * Take() can be used to obtain limited collection of data based on the supplied size parameter.
                 * Example: if ItemPerPage is 5, then it will obtain 5 data from the data list.
                 * If data list contain less than 5 data, then it will take as many available data as possible.
                 */
                civilians = CivilianData.Civilians
                    .Skip((PageIndex - 1) * ItemPerPage)
                    .Take(ItemPerPage)
                    .Select(Q => new CivilianModel
                    {
                        Ktp = Q.Key,
                        Name = Q.Value.Name,
                        Address = Q.Value.Address,
                        PhoneNumber = Q.Value.PhoneNumber,
                        Gender = Q.Value.Gender.ToString(),
                        // Convert DateTime or DateTimeOffset object to a specific date and time format.
                        // Reference: https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings.
                        // Use Indonesia date & time culture. Make sure your OS support this.
                        // Reference: https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings#control-panel-settings.
                        Birthday = Q.Value.Birthday.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture(IndonesiaCultureInfoId)),
                        CreatedAt = Q.Value.CreatedAt.ToString("dd MMM yyyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture(IndonesiaCultureInfoId))
                    })
                    .ToList();
            }
            else
            {
                // Use Stopwatch class to diagnose your performance speed!
                // Reference: https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.stopwatch?view=netframework-4.8.
                var sw = new Stopwatch();
                sw.Start();

                // TryGetValue will try to look up the value of the Dictionary based passed key value.
                // Reference: https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.dictionary-2.trygetvalue?view=netcore-3.1.
                var hasCivilian = CivilianData.Civilians
                    .TryGetValue(SearchString, out var civilian);

                sw.Stop();

                // String interpolation with string format.
                // Reference: https://stackoverflow.com/questions/6951335/using-string-format-to-show-decimal-up-to-2-places-or-simple-integer/6951366.
                // Another reference: https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/string-interpolation#how-to-specify-a-format-string-for-an-interpolation-expression.
                Console.WriteLine($"Search speed: {sw.ElapsedMilliseconds : 0.##} ms");

                if (hasCivilian == true)
                {
                    civilians.Add(new CivilianModel
                    {
                        Ktp = SearchString,
                        Name = civilian.Name,
                        Address = civilian.Address,
                        PhoneNumber = civilian.PhoneNumber,
                        Gender = civilian.Gender.ToString(),
                        Birthday = civilian.Birthday.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture(IndonesiaCultureInfoId)),
                        CreatedAt = civilian.CreatedAt.ToString("dd MMM yyyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture(IndonesiaCultureInfoId))
                    });
                }

                DataGridTotalData = civilians.Count;
            }

            if (civilians.Count > 0)
            {
                var loopIndex = 1 + ((PageIndex - 1) * ItemPerPage);
                foreach (var civilian in civilians)
                {
                    Console.WriteLine($"| {loopIndex} | {civilian.Ktp} | {civilian.Name} | {civilian.Address} | {civilian.PhoneNumber} | {civilian.Gender} | {civilian.Birthday} | {civilian.CreatedAt} |");
                    loopIndex++;
                }
            }
            else
            {
                Console.WriteLine("No civilian data was found");
            }

            Console.WriteLine($@"
Total civilians: {DataGridTotalData}
Current page: {PageIndex}
Search keyword: {SearchString}");
        }

        /// <summary>
        /// Display the available menu options.
        /// </summary>
        public static void DisplayMenus()
        {
            Console.WriteLine(@"
Menus:");
            foreach (var menuOption in MenuOptions)
            {
                Console.WriteLine(menuOption);
            }

            Console.WriteLine("Please input your choice:");
        }

        /// <summary>
        /// Accept user input, validate the input and proceed the input.
        /// </summary>
        public static void AcceptInput()
        {
            var isExitInput = false;

            while (isExitInput == false)
            {
                var isInputNumber = int.TryParse(Console.ReadLine(), out var inputNumber);

                if (isInputNumber == false)
                {
                    Console.WriteLine("Please input a number");
                }
                else
                {
                    if (inputNumber != 6)
                    {
                        switch (inputNumber)
                        {
                            case 1:
                                GoToPage();
                                break;
                            case 2:
                                Search();
                                break;
                            case 3:
                                AddNewEntry();
                                break;
                            case 4:
                                break;
                            case 5:
                                break;
                            default:
                                Console.WriteLine("Please input a valid option");
                                break;
                        }

                        Console.Clear();
                        DisplayMainMenu();
                    }
                    else
                    {
                        Console.WriteLine($"Thank you for using {ApplicationTitle} system!");
                        isExitInput = true;
                    }
                }
            }
        }

        /// <summary>
        /// Go to page feature method. Will change the current page (PageIndex) of the displayed civilian data grid.
        /// </summary>
        public static void GoToPage()
        {
            Console.WriteLine("Please input your number page:");

            var isValidInput = false;

            while (isValidInput == false)
            {
                var isInputNumber = int.TryParse(Console.ReadLine(), out var inputNumber);

                var pageSize = GetTotalPageSize();
                if (isInputNumber == false)
                {
                    Console.WriteLine("Please input a number");
                }
                else if (inputNumber < 1 || inputNumber > pageSize)
                {
                    Console.WriteLine($"Please input a number between 1 and {pageSize}");
                }
                else
                {
                    PageIndex = inputNumber;
                    isValidInput = true;
                }
            }
        }

        /// <summary>
        /// Get the total page size based on the calculation between data grid total data and item per page.
        /// </summary>
        public static int GetTotalPageSize()
        {
            var totalPageSize = (int)Math.Ceiling((decimal)DataGridTotalData / (decimal)ItemPerPage);

            return totalPageSize;
        }

        /// <summary>
        /// Search the civilian data based on the search string input.
        /// </summary>
        public static void Search()
        {
            Console.WriteLine("Please input your search keyword (leave an empty string or blank to reset the current search keyword):");

            var input = Console.ReadLine();

            SearchString = input;

            // Usually, the page index is will be resetted to 1 after submiting a search keyword.
            PageIndex = 1;
        }

        /// <summary>
        /// Add new civilian entry.
        /// </summary>
        public static void AddNewEntry()
        {
            Console.WriteLine("Please input the person's KTP:");
            var ktp = Console.ReadLine();

            Console.WriteLine("Please input the person's Name:");
            var name = Console.ReadLine();

            Console.WriteLine("Please input the person's Address:");
            var address = Console.ReadLine();

            Console.WriteLine("Please input the person's Phone Number:");
            var phoneNumber = Console.ReadLine();

            Console.WriteLine("Please input the person's Gender (just input either 1 (for Male) or 2 (for Female)):");
            var isValidGender = false;
            var gender = Gender.Male;

            while (isValidGender == false)
            {
                var isInputNumber = int.TryParse(Console.ReadLine(), out var inputNumber);

                if (isInputNumber == false)
                {
                    Console.WriteLine("Please input a number");
                }
                // It is safe to cast an enum into an int.
                else if ((inputNumber - 1) < (int)Gender.Male || (inputNumber - 1) > (int)Gender.Female)
                {
                    Console.WriteLine($"Please input a number between 1 and 2");
                }
                else
                {
                    // It is also safe to cast an int into an enum.
                    gender = (Gender)inputNumber - 1;
                    isValidGender = true;
                }
            }

            Console.WriteLine("Please input the person's birthday (in yyyyMMdd format):");
            var isValidBirthday = false;
            var birthday = new DateTime();

            while (isValidBirthday == false)
            {
                // Try parse the user birthday input. The input format must be exact as the defined format in this method call.
                // Reference: https://stackoverflow.com/questions/4766845/yyyymmdd-date-format-regular-expression-to-validate-a-date-in-c-sharp-net.
                // Another Reference: https://docs.microsoft.com/en-us/dotnet/api/system.datetime.tryparseexact?view=netcore-3.1.
                var isValidBirthdayInput = DateTime.TryParseExact(Console.ReadLine(),
                    "yyyyMMdd",
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out var birthdayInput);

                if (isValidBirthdayInput == false)
                {
                    Console.WriteLine($"Please input a valid date format");
                }
                else
                {
                    birthday = birthdayInput;
                    isValidBirthday = true;
                }
            }

            CivilianData.Civilians.Add(ktp,
                new CivilianDataModel
                {
                    Name = name,
                    Address = address,
                    PhoneNumber = phoneNumber,
                    Gender = gender,
                    Birthday = birthday,
                    CreatedAt = DateTimeOffset.UtcNow
                });

            PageIndex = 1;
        }
    }
}
